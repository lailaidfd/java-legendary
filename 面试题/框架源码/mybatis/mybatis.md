https://www.cnblogs.com/chenpi/p/10498921.html

![img](./images/879896-20190308234940881-593104961.png)

mybatis插件实际被描述为拦截器更为准确，其拦截的是mybatis四大核心组件，包括：

1. executor：执行语句、进行提交，回滚、查询、更新
2. statementHandler：语法构建，拦截此处可以获取原始sql，修改分页参数
3. parameterHandler:入参解析
4. resultSetHandler：结果集解析

原理：

核心：拦截器内有 intercept方法、plugin方法。

mybatis插件的原理就是动态代理，真正执行这几个核心组件的类对象实际都是代理对象。

mybatis会扫描所有拦截器，生成代理对象，如果有多个拦截器，就再加一层代理对象，比如两个插件A，B，A代理原始handler之后B是会再动态代理A的。这里反映了一个责任链模式，打破责任连可以不调用invocation#proceed。



