[TOC]

> author：编程界的小学生
>
> date：2021/03/23

# 1、SpringBean的作用域

![SpringBean-1.png](images/SpringBean-1.png)

# 2、SpringBean的生命周期

![SpringBean-2.png](images/SpringBean-2.png)

- 实例化一个Bean，也就是我们常说的new

- 按照Spring上下文对实例化的Bean进行配置，也就是IOC注入

- 如果这个Bean已经实现了BeanNameAware接口，会调用它实现的setBeanName(String)方法，此处传递的就是Spring配置文件中Bean的id值

- 如果这个Bean已经实现了BeanFactoryAware接口，会调用它实现的setBeanFactory(setBeanFactory(BeanFactory)传递的是Spring工厂自身

- 如果这个Bean已经实现了ApplicationContextAware接口，会调用setApplicationContext(ApplicationContext)方法，传入Spring上下文

- 如果这个Bean关联了BeanPostProcessor接口，将会调用postProcessBeforeInitialization(Object obj, String s)方法
- 如果Bean类已实现org.springframework.beans.factory.InitializingBean接口，则执行他的afterProPertiesSet()方法。

- 如果Bean在Spring配置文件中配置了init-method属性会自动调用其配置的初始化方法。

- 如果这个Bean关联了BeanPostProcessor接口，将会调用postProcessAfterInitialization(Object obj, String s)方法。

- 当Bean不再需要时，会经过清理阶段，如果Bean实现了DisposableBean这个接口，会调用那个其实现的destroy()方法。

- 最后，如果这个Bean的Spring配置中配置了destroy-method属性，会自动调用其配置的销毁方法。

# 3、Spring IOC原理

**入口是refresh方法。**

1. 执行 prepareRefresh() 方法。进行设置初始化资源、事件监听器、初始化早期事件集合变量；
2. 执行 obtainFreshBeanFactory() 方法。创建默认的 bean 工厂 DefaultListableBeanFactory，创建的同时就加载了 bean 定义信息；
3. 执行 prepareBeanFactory(beanFactory) 方法。进行预处理 bean 工厂，设置 bean 工厂的一些属性，包括一些后置处理器、默认环境 bean 等；
4. 执行 postProcessBeanFactory(beanFactory) 方法。后置处理 bean 工厂，注册一些特定的后置处理器、环境单例 bean 等；
5. 执行 invokeBeanFactoryPostProcessors(beanFactory) 方法。它会执行 bean 工厂后置处理器、注册 bean 定义、注入增强 bean 类等，非常重要的一步操作；
6. 执行 registerBeanPostProcessors(beanFactory) 方法。进行注册后置处理器；
7. 执行 initMessageSource() 方法。初始化消息源；
8. 执行 initApplicationEventMulticaster() 方法。初始化程序事件多播器，多播器用来广播事件的；
9. 执行 onRefresh() 方法。初始化其他在特定环境订阅下的特定的 bean；
10. 执行 registerListeners() 方法。注册监听器；
11. 执行 finishBeanFactoryInitialization() 方法。实例化所有剩余的（非延迟初始化）单例，非常重要的一步操作；
12. 执行 finishRefresh() 方法。刷新发布相应的事件。

![springioc-1.png](images/springioc-1.png)

# 4、Spring AOP原理

**简单说说 AOP 的设计**

1. 每个 Bean 都会被 JDK 或者 Cglib 代理。取决于是否有接口（实现了接口就是jdk动态代理）。
2. 每个 Bean 会有多个“方法拦截器”。注意：拦截器分为两层，外层由 Spring 内核控制流程，内层拦截器是用户设置，也就是 AOP。
3. 当代理方法被调用时，先经过外层拦截器，外层拦截器根据方法的各种信息判断该方法应该执行哪些“内层拦截器”。内层拦截器的设计就是职责连的设计。

**AOP的具体流程**

**1、代理的创建**

- 首先，需要创建代理工厂，代理工厂需要 3 个重要的信息：拦截器数组，目标对象接口数组，目标对象。
- 创建代理工厂时，默认会在拦截器数组尾部再增加一个默认拦截器 —— 用于最终的调用目标方法。
- 当调用 getProxy 方法的时候，会根据接口数量大余 0 条件返回一个代理对象（JDK or Cglib）。
- 注意：创建代理对象时，同时会创建一个外层拦截器，这个拦截器就是 Spring 内核的拦截器。用于控制整个 AOP 的流程。

**2、代理的调用**

- 当对代理对象进行调用时，就会触发外层拦截器。
- 外层拦截器根据代理配置信息，创建内层拦截器链。创建的过程中，会根据表达式判断当前拦截是否匹配这个拦截器。而这个拦截器链设计模式就是职责链模式。
- 当整个链条执行到最后时，就会触发创建代理时那个尾部的默认拦截器，从而调用目标方法。最后返回。

![springaop-1.png](images/springaop-1.png)

**关于调用过程，来张流程图**

![springaop-2.png](images/springaop-2.png)

# 5、BeanFactory和FactoryBean的区别

- BeanFactory是SpringIOC最基本的根容器，提供了IOC的基础功能，是Bean的顶层工厂，常用的ClassPathXmlApplicationContext等都是BeanFactory的子孙类。负责创建对象和管理对象。
- BeanFactory里面的&符号作用是获取FactoryBean本身，而不是由FactoryBean创建出来的那个对象（getObject方法返回的对象）。

- FactoryBean是工厂Bean，和普通 Bean不一样，FactoryBean返回的对象不是指定类的一个实例，而是由FactoryBean的getObject()方法所返回的对象。
- FactoryBean本身存活在BeanFactory当中，他本身也是一个工厂，这个工厂作用用于获取由FactoryBean所能创建出来的那个对象（getObject方法），在创建出来对象的时候，可以在这个对象方法的前面或者后面可以额外的执行一些操作，他是实现AOP的一个重要的基石。

[https://blog.csdn.net/ctwctw/article/details/101735661](https://blog.csdn.net/ctwctw/article/details/101735661)

# 6、Spring事务原理

Spring容器在初始化每个单例bean的时候，会遍历容器中的所有BeanPostProcessor实现类，并执行其postProcessAfterInitialization方法，在执行AbstractAutoProxyCreator类的postProcessAfterInitialization方法时会遍历容器中所有的切面，查找与当前实例化bean匹配的切面，这里会获取事务属性切面，查找@Transactional注解及其属性值，然后根据得到的切面创建一个代理对象，默认是使用JDK动态代理创建代理，如果目标类是接口，则使用JDK动态代理，否则使用Cglib。在创建代理的过程中会获取当前目标方法对应的拦截器，此时会得到TransactionInterceptor实例，在它的invoke方法中实现事务的开启和回滚，在需要进行事务操作的时候，Spring会在调用目标类的目标方法之前进行开启事务、调用异常回滚事务、调用完成会提交事务。是否需要开启新事务，是根据@Transactional注解上配置的参数值来判断的。如果需要开启新事务，获取Connection连接，然后将连接的自动提交事务改为false，改为手动提交。当对目标类的目标方法进行调用的时候，若发生异常将会进入completeTransactionAfterThrowing方法。 

**能否通俗的讲述一下它的实现原理？**

如果在类A上标注Transactional注解，Spring容器会在启动的时候，为类A创建一个代理类B，类A的所有public方法都会在代理类B中有一个对应的代理方法，调用类A的某个public方法会进入对应的代理方法中进行处理；如果只在类A的b方法(使用public修饰)上标注Transactional注解，Spring容器会在启动的时候，为类A创建一个代理类B，但只会为类A的b方法创建一个代理方法，调用类A的b方法会进入对应的代理方法中进行处理，调用类A的其它public方法，则还是进入类A的方法中处理。在进入代理类的某个方法之前，会先执行TransactionInterceptor类中的invoke方法，完成整个事务处理的逻辑，如是否开启新事务、在目标方法执行期间监测是否需要回滚事务、目标方法执行完成后提交事务等。

# 7、Spring是如何解决循环依赖的

采取三个缓存的方式来解决。

- singletonObjects：第一级缓存，里面存放的都是创建好的`成品Bean`。

- earlySingletonObjects  : 第二级缓存，里面存放的都是`半成品的Bean`。

- singletonFactories ：第三级缓存， 不同于前两个存的是 Bean对象引用，此缓存存的bean 工厂对象，也就存的是 专门创建Bean的一个工厂对象。此缓存用于解决循环依赖。

具体步骤如下：

- A 创建过程中需要 B，于是 **A 将自己放到三级缓（singletonFactories ）里面** ，去实例化 B
- B 实例化的时候发现需要 A，于是 B 先查一级缓存，没有，再查二级缓存，还是没有，再查三级缓存，找到了！
- - **然后把三级缓存里面的这个 A 放到二级缓存里面，并删除三级缓存里面的 A**
  - B 顺利初始化完毕，**将自己放到一级缓存里面**（此时B里面的A依然是创建中状态）
- 然后回来接着创建 A，此时 B 已经创建结束，直接从一级缓存里面拿到 B ，然后完成创建，**并将自己放到一级缓存里面**
- 如此一来便解决了循环依赖的问题

singletonsCurrentlyInCreation 是一个set，存放正在初始化的bean



# 8、常用的PostProcessor和顺序

1. BeanDefinitionRegistarPostProcessor:扩展自BeanFactoryPostProcessor，专门用于动态注册Bean,内部可以调用注册器注册BD并将已经进入容器的bean设置到类属性。

2. BeanFactoryPostProcessor:是对bean初始化前的一个统一处理。postProcessBeanFactory方法可以在spring的bean创建之前，修改bean的定义属性但是beanPostProcessor不行。不过不要在这里实例化bean。

3. BeanPostProcessor:存在postProcessAfterInitialization，postProcessBeforeInitialization，容器中所有bean在初始化都会调用这个类的方法，它可以在bean初始化前后获取信息，但是不能改。不过你可以在这里面判断之后去返回一个代理对象，springAOP的代理对象就是这么来的。

### Spring事务原理

spring事务涉及知识很多，spring事务具有5种隔离级别，7种传播机制。

spring使用AOP来实现声明式事务、而AOP也分为JDK和CGLIB动态代理。

- 增强：

  AspectJ可以对字段、类等进行增强，spring默认只能对bean方法进行增强、常用前置、后置、返回@AfterReturning（出现exception不执行）、环绕、异常增强@AfterThrowing。

  **执行顺序为 前置增强----->目标函数------->后置增强-------->返回增强跟异常增强二选一**

  声明式事务就是通过环绕增强、在目标方法执行前开启事务、结束后关闭事务实现的。

- 代理对象：

  是通过BeanPostProcessor接口，在bean初始化完成之后生成。

- 获取上下文事务：

  实际上事务存在ThreadLocal里面

### Spring AOP术语

- 通知Advice：

  通知就是站在宏观的层面看你这次AOP想增加的功能，比如日志、事务、安全等。

- 连接点 JoinPoint

  连接点就是AOP框架允许你通知的地方，指得就是所有方法的前、后、前后、抛出异常等位置，一个方法就有4个（spring AOP、Aspectj更多）。

  ``` @Around("pointCut()")```

- 切入点 Pointcut

  切入点定义你想要具体用哪些方法，因为你不想在所有方法上都使用通知，说白了就是用来筛选连接点的**方法**的。

  ``` @Pointcut("execution(* com.xxx.xxx.controller.*.*(..))")```

- 切面 Aspect

  切入点做的事情。

- 目标 target

  就是被代理的对象、被通知的对象，真正做自身业务的小可怜。

### spring事务失效情况

- 方法不是public
- 异常没有rollback捕获或者被catch了
- 自调用

### spring如何每次保证获取的是同一个事务

```java
// AbstractAutoProxyCreator.class
@Override
public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    if (bean != null) {
        Object cacheKey = getCacheKey(bean.getClass(), beanName);
        if (!this.earlyProxyReferences.contains(cacheKey)) {
            // 创建代理对象
            return wrapIfNecessary(bean, beanName, cacheKey);
        }
    }
    return bean;
}
```

最后是通过调用`ProxyFactory#getProxy(java.lang.ClassLoader)`方法来创建代理对象：

Spring是如何知道当前是否已经开启了事务呢

`TransactionSynchronizationManager`通过`ThreadLocal`对象在当前线程记录了`resources`和`synchronizations`属性。`resources`是一个HashMap，用于记录当前参与事务的事务资源，方便进行事务同步，在`DataSourceTransactionManager`的例子中就是以`dataSource`作为key，保存了数据库连接，这样在同一个线程中，不同的方法调用就可以通过`dataSource`获取相同的数据库连接，从而保证所有操作在一个事务中进行。

### SpringBoot 四大核心

1. starter

   - 装配：SPI，放在你的spring.factories，org.springframework.boot.autoconfigure.EnableAutoConfiguration=你的@Configuration

   - 提示：@ConfigurationProperties

     扫描得到你的配置，配合@Import

     不同条件创建不同Bean，@ConditionalXXX

### 聊一下@SpringBootApplication

涉及到EnableAutoConfiguration

ComponentScan：默认扫描的本类下的包

AutoConfigurationPackage：将当前包下的所有组件都扫描到spring容器

Spring启动的时候会扫描所有jar路径下的`META-INF/spring.factories`，将其文件包装成Properties对象并最后添加到容器中。

### 获取spring容器对象

1. BeanFactoryAware
2. ApplicationContextAware
3. ApplicationListener

### Bean初始化执行顺序

1. PostConstruct
2. InitializingBean
3. init-method接口

### @Async 原理

### ResourceLoaderAware

将资源或文件(例如文本文件、XML文件、属性文件或图像文件)加载到Spring应用程序上下文中的不同实现，比如spring自身的外部配置。

### mybatis的Mapper接口是怎么被实例化的

Spring后续在实例化这个Mapper接口的时候，会通过`FactoryBean`进行动态代理实例化并返回。

![./images/spring-mybatis.jpg](./images/spring-mybatis.jpg)



![img](https://awps-assets.meituan.net/mit-x/blog-images-bundle-2018a/6e38df6a.jpg)

每个SqlSession中持有了Executor，每个Executor中有一个LocalCache。当用户发起查询时，MyBatis根据当前执行的语句生成`MappedStatement`，在Local Cache进行查询，如果缓存命中的话，直接返回结果给用户，如果缓存没有命中的话，查询数据库，结果写入`Local Cache`，最后返回结果给用户。具体实现类的类关系图如下图所示。

![img](https://awps-assets.meituan.net/mit-x/blog-images-bundle-2018a/bb851700.png)

`BaseExecutor`成员变量之一的`PerpetualCache`，是对Cache接口最基本的实现，其实现非常简单，内部持有HashMap，对一级缓存的操作实则是对HashMap的操作。

一级缓存

1. MyBatis一级缓存的生命周期和SqlSession一致。
2. MyBatis一级缓存内部设计简单，只是一个没有容量限定的HashMap，在缓存的功能性上有所欠缺。
3. MyBatis的一级缓存最大范围是SqlSession内部，有多个SqlSession或者分布式的环境下，数据库写操作会引起脏数据，**建议设定缓存级别为Statement**。

二级缓存

1. MyBatis的二级缓存相对于一级缓存来说，实现了`SqlSession`之间缓存数据的共享，同时粒度更加的细，能够到`namespace`级别，通过Cache接口实现类不同的组合，对Cache的可控性也更强。
2. MyBatis在多表查询时，极大可能会出现脏数据，有设计上的缺陷，安全使用二级缓存的条件比较苛刻。
3. 在分布式环境下，由于默认的MyBatis Cache实现都是基于本地的，分布式环境下必然会出现读取到脏数据，需要使用集中式缓存将MyBatis的Cache接口实现，有一定的开发成本，直接使用Redis、Memcached等分布式缓存可能成本更低，安全性也更高。
