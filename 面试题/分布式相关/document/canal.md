[TOC]

> author：编程界的小学生
>
> date：2021/03/16

# 一、Canal原理

![canal-1.png](images/canal-1.png)

- 首先第一个步跟上面主从复制原理一样
- Canal会伪装成Mysql的Slave服务器,向Mysql的Master服务器传输dump协议 
- Master服务器接受到Dump请求后,开通推送BinLog日志给Slave服务器（也就是Canal服务端），解析BinLog对象(原始为Byte流) ,转成JSON格式 
- Cannal客户端,可以有两种方式来监听服务端(TCP协议/MQ),但是最好是通过MQ形式（目前支持KafKa,RocketMQ ）,发送JSON数据到Server端,消费者监听到消 息,消费即可（MQ集群需要考虑消息顺序消费性问题）

# 二、canal-server架构

![canal-2](images/canal-2.webp)

**Canal的Server端本身根据配置启动了很多个Instance对象，所谓的Instance对象就是模拟的数据库slave和master进行连接，换句话说就是假设canal同时成为N个数据库的slave，那么就会有N个Instance实例。**

# 三、源码入口

`com.alibaba.otter.canal.deployer.CanalLauncher#main()`