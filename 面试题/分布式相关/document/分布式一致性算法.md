[TOC]

> author：编程界的小学生
>
> date：2021/03/17

# 零、分为最终一致性和强一致性

**最终一致性**

- Gossip

**强一致性**

- 同步：比如MySQL主从复制可以配置成要等到slave都返回ok后才会响应客户端。若其中一个slave宕机了，则Master阻塞，导致整个集群不可用，违背了CAP的A，可用性，但是保证了强一致性C。

- raft
- zab
- paxos

**分类**

![paxos-1.png](images/paxos-1.png)

# 一、Raft

**原文参考：**[https://juejin.cn/post/6856630564276371470#heading-20](https://juejin.cn/post/6856630564276371470#heading-20)

**在线动画：**[http://thesecretlivesofdata.com/raft/](http://thesecretlivesofdata.com/raft/)

## 1、核心知识

### 1.1、是什么？

**Raft是一个用于管理日志一致性的协议**。它将分布式一致性分解为多个子问题：**Leader选举（Leader election）、日志复制（Log replication）、安全性（Safety）、日志压缩（Log compaction）等**

### 1.2、角色

Leader：接受客户端请求，并向Follower同步请求日志，当日志同步到大多数节点上后告诉Follower提交日志

Follower：接受并持久化Leader同步的日志，在Leader告之日志可以提交之后，提交日志

Candidate：Leader选举过程中的临时角色

### 1.3、Term

**Raft 算法将时间划分成为任意不同长度的任期（term）**。任期用连续的数字进行表示。**每一个任期的开始都是一次选举（election），一个或多个候选人会试图成为领导人**。如果一个候选人赢得了选举，它就会在该任期的剩余时间担任领导人。在某些情况下，选票会被瓜分，有可能没有选出领导人，那么，将会开始另一个任期，并且立刻开始下一次选举。**Raft 算法保证在给定的一个任期最多只有一个领导人**。

### 1.4、RPC

**Raft 算法中服务器节点之间通信使用远程过程调用（RPC）**，并且基本的一致性算法只需要两种类型的 RPC，为了在服务器之间传输快照增加了第三种 RPC。

【RPC有三种】：

- **RequestVote RPC**：**候选人在选举期间发起**。
- **AppendEntries RPC**：**领导人发起的一种心跳机制，复制日志也在该命令中完成**。
- **InstallSnapshot RPC**: 领导者使用该RPC来**发送快照给太落后的追随者**。

## 2、Leader选举流程

**Raft 使用心跳（heartbeat）触发Leader选举。如果Follower在选举超时时间内没有收到Leader的heartbeat，就会等待一段随机的时间后发起一次Leader选举**。

term先结束变成候选人状态，然后发起投票。都同意后，term归0。每次心跳正常后follower都会把term归0，相当于没心跳的时候你时时刻刻想着造反，心跳就是监督，打消你这个念头，直接让你归0。 如果心跳时间超出了term转完一圈的时间，那么就代表leader挂了，重新选举（走上面的流程，term先结束的变成候选人重新发起投票），得到票数多的follower升级为leader。如果是偶数个，可能会出现平票的 现象，平票的话代表此次选举作废，进入下一轮选举。

## 3、日志复制过程

**Leader选出后，就开始接收客户端的请求**。**Leader把请求作为日志条目加入到它的日志中，然后并行的向其他服务器发起RPC复制日志条目**。当这条日志被复制到过半数服务器上，Leader将这条日志应用到它的状态机并向客户端返回执行结果。

- 客户端的每一个请求都包含被复制状态机执行的指令。
- **leader把这个指令作为一条新的日志条目添加到日志中，然后并行发起 RPC 给其他的服务器，让他们复制这条信息**。
- 假如这条日志被安全的复制，领导人就应用这条日志到自己的状态机中，并返回给客户端。
- **如果 follower 宕机或者运行缓慢或者丢包，leader会不断的重试，直到所有的 follower 最终都复制了所有的日志条目**。

## 4、一致性如何实现？

主要是通过日志复制实现数据一致性，leader将请求指令作为一条新的日志条目添加到日志中，然后发起RPC 给所有的follower，进行日志复制，进而同步数据。

# 二、Zab

**原文链接：**[https://juejin.cn/post/6844903816937095176](https://juejin.cn/post/6844903816937095176)

## 1、什么是Zab？

zab协议是zookeeper专门设计的支持崩溃恢复的原子广播协议。目的是实现分布式zoopkeeper各个节点数据一致性。

## 2、zab是怎么实现分布式数据一致性的？

zab协议约定zk节点有两种角色：leader和follower。zk客户端会随机的连接到zk集群中的一个节点，若是读请求，就直接从当前节点读数据，若是写请求，那么节点就会向leader提交事务，leader接收到事务请求提交后会广播该事务给其他follower，只要超过半数节点写入成功该事务就会被提交。

## 3、leader选举

leader只有一个，挂了后follower需要选举为leader。

假设当前有zk1，zk2，zk3三个节点组成一个集群，现在zk3节点挂了，那么选举流程如下：

- 1、首先zk1和zk2都会将自己作为leader服务器来进行投票。每次投票会包含所推举的服务器的myid和zxid，使用(myid,zxid)来表示，假设此时zk1的投票为(1,1)，zk2的投票为(2,1)，然后各自将这个投票发给集群中其他机器。
- 2、各服务器接收到投票之后会进行检查（如：检查是否是本轮投票、是否来自LOOKING状态的服务器）
- 3、开始处理投票，服务器都需要将别人的投票和 自己的myid和zxid进行比较，规则是优先检查zxid，zxid比较大的服务器优先作为leader，若zxid相同，那么比较myid，myid较大的服务器作为leader服务器。比如上面的案例中，zxid一样，那么明显myid是2的那个follower（也就是zk2）升级为leader。

### 3.1、追问：重新选举的时候怎么保证数据不丢失的？

zk是cp的，数据肯定不会丢失的。 在选举过程中，不能对外提供服务，但在选举过程中，首先zxid最大的为leader，zxid最大，表示数据是最新的，然后广播给其他follower，这样避免数据丢失。

# 三、Paxos

**推荐文章：**[https://liu-jianhao.github.io/2019/05/paxosmulti-paxos%E8%AF%A6%E8%A7%A3/](https://liu-jianhao.github.io/2019/05/paxosmulti-paxos%E8%AF%A6%E8%A7%A3/)

## 1、核心知识

### 1.1、核心理念

paxos只是一个协议，不是具体的一套解决方案。

核心思想是**多数派写**，少数服从多数，过半【(2 / n) + 1】选举。

### 1.2、角色

- proposer：提议发起者，Proposer 可以有多个，Proposer 提出议案（value）。所谓 value，可以是任何操作，比如“设置某个变量的值为value”。不同的 Proposer 可以提出不同的 value，例如某个Proposer 提议“将变量 X 设置为 1”，另一个 Proposer 提议“将变量 X 设置为 2”，但对同一轮 Paxos过程，最多只有一个 value 被批准
- acceptor：提议接受者，Acceptor 有 N 个，Proposer 提出的 value 必须获得超过半数(N/2+1)的 Acceptor批准后才能通过。Acceptor 之间完全对等独立。
- learner：提议学习者，上面提到只要超过半数accpetor通过即可获得通过，那么learner角色的目的就是把通过的确定性取值同步给其他未确定的Acceptor。

### 1.3、协议过程

proposer将发起提案（value）给所有acceptor，超过半数acceptor获得批准后，proposer将提案写入acceptor内，最终所有acceptor获得一致性的确定性取值，且后续不允许再修改。具体细节如下：

- 每个proposer在提案的时候，首先需要获取一个全局唯一，递增的编号N，将N赋给提案。N的生成方式有两种，全局性生成器、提案者自身维护。

- 每个acceptor在接收到提案之后，会把提案编号保存在本地。这样每个表决者都有一个最大编号，假设是maxN。而每个表决者只会accept编号大于自己本地maxN的提案。

- 最终只有一个提案被选定

- 一旦一个提案被选定，那么learner会同步该提案到本地。

- 没有提案选出就不会有提案被选定

## 2、basic paxos

### 2.1、选举过程（两阶段）

**prepare阶段**

提案者准备一个编号N的提议，然后向决策者发送prepare(N)，用于试探集群是否支持该提案。

每个决策者本地都有一个maxN，当接收到prepare(N)的时候，会与自己的maxN进行比较，有几种情况。

- 若N小于maxN，说明该提案已经过时，那么就不回应或者回应error，来拒绝该提案。
- 若N大于maxN，说明决策者可以接收该提案，会返回一个Proposal(myid,maxN,value)，曾经接收的最大maxN的提案，来表示自己愿意接收该提案。myid就是该提案者的id，maxN就是该提案的编号，value就是提案的值。当然，如果表决者没有接受过提案，则会将提案Proposal(myid,null,null)反馈给提议者。

**accept阶段**

1. 当提议者发出prepare(N)后,若收到超过多数的表决者的响应，那么该提议者就会将真正的提案Proposal(N,value)发送给所有表决者
2. 当表决者接受到提议者发送的Proposer(N,value)后，会再次拿出自己曾经接受过的提议中最大的标号maxN,以及曾经反馈过prepare最大编号，当N小于这两个编号的时候，则不响应拒绝或error形式拒绝，当N大于这个两个编号，表决者接受此提案，并反馈给提议者
3. 若提议者没有收到多数的的反馈，则重新进入prepare,递增提案编号，重新提交提案，若超过多数表决者的反馈，则其他未向提议者反馈的接受的表决者成为learner,主动同步 提议者的提案。

![paxos-2.png](images/paxos-2.png)

### 2.2、问题

- 活锁问题

> 活锁问题就是  一个client先发起请求，N为1，然后proposer收到后还没处理完，client再次发起了一个请求（因为proposer是高可用的，再次发请求可能由其他proposer发起的），N为2了，这时候N为1的请求处理完了返回给proposer的时候，发现N现在到2了，则放弃这个1的，重新再发一个N为3的请求，但是2返回的时候发现是3了，再发个4的，活锁！

- 两阶段都是rpc调用，次数太多

## 3、multi paxos

### 3.1、提高效率

- Basic Paxos 是低效的
  1. 多个 proposers 冲突问题
  2. 对于选中一个值需要两轮RPCs(Prepare、Accept)
- 解决：
  1. 选一个 leader
     - 在一个时刻，只有一个 Proposer
  2. 消除大部分 Prepare RPCs
     - 一轮 Prepare 完整的日志，而不是单条记录。
     - 一旦完成了 Prepare，它就可以通过 Accept 请求，同时创建多条记录。这样就不需要多次使用 Prepare 阶段。这样就能减少一半的 RPCs。

### 3.2、领导选举

选举领导者的方式有很多，这里只介绍一种由 Leslie Lamport 建议的简单方式。

- 让有 **最高 ID** 的服务器作为领导者
- 可以通过每个服务器定期（每 T ms）向其他服务器 **发送心跳消息** 的方式来实现。这些消息包含发送服务器的 ID
- 如果它们没有能收到某一具有高 ID 的服务器的心跳消息，这个间隔（通常是 2T ms）需要设置的足够长，让消息有足够的通信传递时间。所以，如果这些服务器没有能接收到高 ID 的服务器消息，然后它们会自己选举成为领导者。
- 也就是说，首先它会从客户端接受到请求，其次在 Paxos 协议中，它会 **同时扮演 proposer 和 acceptor**
- 如果机器能够接收到来自高 ID 的服务器的心跳消息，它就不会作为 leader，如果它接收到客户端的请求，那么它会 **拒绝** 这个请求，并告知客户端与 leader 进行通信。
- 非 leader 服务器不会作为 proposer，**只会作为 acceptor**
- 这个机制的优势在于，它不太可能出现两个 leader 同时工作的情况，即使这样，如果出现了两个 leader，Paxos 协议还是能正常工作，只是不是那么高效而已。
- 应该注意的是，实际上大多数系统都不会采用这种选举方式，它们会采用基于 **租约** 的方式（lease based approach），这比上述介绍的机制要复杂的多，不过也有其优势。

### 3.3、减少 Prepare 的 RPC 请求

- 为什么需要 Prepare？
  - 需要使用提议序号来 **阻止** 旧的提议
  - 使用 Prepare 阶段来检查已经被接受的值，这样就可以使用这些值来替代原本自己希望接受的值。

1. 第一个问题是阻止所有的提议，我们可以通过改变提议序号的含义来解决这个问题， **将提议序号全局化，代表完整的日志** ，而不是为每个日志记录都保留独立的提议序号。这么做要求我们在完成一轮准备请求后，当然我们知道这样做会锁住整个日志，所以后续的日志记录不需要其他的准备请求。
2. 第二个问题有点讨巧。因为在不同接受者的不同日志记录里有很多接受值，为了处理这些值，我们扩展了准备请求的返回信息。和之前一样，准备请求仍然返回 acceptor 所接受的最高 ID 的提议，它只对当前记录这么做，不过除了这个， acceptor 会查看当前请求的后续日志记录，**如果后续的日志里没有接受值** ，它还会返回这些记录的标志位 **noMoreAccepted** 。
3. 使用了这种领导者选举的机制，领导者会达到一个状态，每个 acceptor 都返回 noMoreAccepted ，领导者知道所有它已接受的记录。所以一旦达到这个状态，对于单个 acceptor 我们不需要再向这些 acceptor 发送准备请求，因为它们已经知道了日志的状态。
4. 不仅如此，一旦从集群 **大多数 acceptor 那获得 noMoreAccepted 返回值** ，我们就 **不需要发送准备的 RPC 请求** 。也就是说， leader 可以简单地发送 Accept 请求，只需要一轮的 RPC 请求。这个过程会一直执行下去，唯一能改变的就是有其他的服务器被选举成了 leader ，当前 leader 的 Accept 请求会被拒绝，整个过程会重新开始。

# 四、Gossip

来源：[https://www.cnblogs.com/charlieroro/articles/12655967.html](https://www.cnblogs.com/charlieroro/articles/12655967.html)

## 1、是什么

是一个通信协议，一种传播消息的方式。主要用途就是**信息传播和扩散**，基于Gossip协议的一些有名的系统：Redis（Cluster模式），Consul等。

## 2、原理

Gossip协议基本思想就是：一个节点想要分享一些信息给网络中的其他的一些节点。于是，它**周期性**的**随机**选择一些节点，并把信息传递给这些节点。这些收到信息的节点接下来会做同样的事情，即把这些信息传递给其他一些随机选择的节点。一般而言，信息会周期性的传递给N个目标节点，而不只是一个。

## 3、图解

图中的公式表示Gossip协议把信息传播到每一个节点需要多少次循环动作，需要说明的是，公式中的20表示整个集群有20个节点，4表示某个节点会向4个目标节点传播消息：

![gossip-1.png](images/gossip-1.png)

如下图所示，红色的节点表示其已经“受到感染”，即接下来要传播信息的源头，连线表示这个初始化感染的节点能正常连接的节点（其不能连接的节点只能靠接下来感染的节点向其传播消息）。并且N等于4，我们假设4根较粗的线路，就是它第一次传播消息的线路：

![gossip-2.png](images/gossip-2.png)

第一次消息完成传播后，新增了4个节点会被“感染”，即这4个节点也收到了消息。这时候，总计有5个节点变成红色：

![gossip-3.png](images/gossip-3.png)

那么在下一次传播周期时，总计有5个节点，且这5个节点每个节点都会向4个节点传播消息。最后，经过3次循环，20个节点全部被感染（都变成红色节点），即说明需要传播的消息已经传播给了所有节点：

![gossip-4.png](images/gossip-4.png)

需要说明的是，20个节点且设置fanout=4，公式结果是2.16，这只是个近似值。真实传递时，可能需要3次甚至4次循环才能让所有节点收到消息。这是因为每个节点在传播消息的时候，是随机选择N个节点的，这样的话，就有可能某个节点会被选中2次甚至更多次。

**总结**

由前面对Gossip协议图解分析可知，节点传播消息是周期性的，并且**每个节点有它自己的周期**。另外，节点发送消息时的**目标节点数**由参数fanout决定。至于往哪些目标节点发送，则是**随机**的。一旦消息被发送到目标节点，那么目标节点也会被感染。一旦某个节点被感染，那么它也会向其他节点传播消息，试图感染更多的节点。最终，每一个节点都会被感染，即消息被同步给了所有节点。

## 4、可扩展性

Gossip协议是可扩展的，因为它只需要O(logN) 个周期就能把消息传播给所有节点。某个节点在往固定数量节点传播消息过程中，并不需要等待确认（ack），并且，即使某条消息传播过程中丢失，它也不需要做任何补偿措施。打个比方，某个节点本来需要将消息传播给4个节点，但是由于网络或者其他原因，只有3个消息接收到消息，即使这样，这对最终所有节点接收到消息是没有任何影响的。

如下表格所示，假定fanout=4，那么在节点数分别是20、40、80、160时，消息传播到所有节点需要的循环次数对比，在节点成倍扩大的情况下，循环次数并没有增加很多。所以，Gossip协议具备可扩展性：

![gossip-5.png](images/gossip-5.png)

## 5、容错性&健壮性

Gossip也具备失败容错的能力，即使网络故障等一些问题，Gossip协议依然能很好的运行。因为一个节点会**多次**分享某个需要传播的信息，即使不能连通某个节点，其他被感染的节点也会尝试向这个节点传播信息。

Gossip协议下，没有任何扮演特殊角色的节点（比如leader等）。任何一个节点无论什么时候下线或者加入，并不会破坏整个系统的服务质量。

然而，Gossip协议也有不完美的地方，例如，**拜占庭**问题（Byzantine）。即，如果有一个恶意传播消息的节点，Gossip协议的分布式系统就会出问题。

